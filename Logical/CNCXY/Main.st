PROGRAM _INIT
    
	MpCnc2Axis_0.Enable := TRUE;

	MpCnc2Axis_0.Override := 100.0;
	CncParameters.ProgramName := 'CncProg';
     
	
	McAcpAxRestorePosY;
	
	MappMotionConfiguration;
END_PROGRAM

PROGRAM _CYCLIC
    
	MpAxisBasic_0.MpLink := ADR(gAxisQX);
	MpAxisBasic_0();
	
	MpAxisBasic_1.MpLink := ADR(gAxisQY);
	MpAxisBasic_1.Parameters := ADR(parametersQY);
	MpAxisBasic_1();
	
	MpCnc2Axis_0.MpLink := ADR(gCncXY);
	MpCnc2Axis_0.Parameters := ADR(CncParameters);
	MpCnc2Axis_0();
	
	MC_BR_ProcessParam_0.Component := ADR(gAxisQX);
	MC_BR_ProcessParam_0.DataType := mcCFG_STP_AX_CTRL;
	MC_BR_ProcessParam_0.DataAddress := ADR(mcCfgStpAxCtrl);
	MC_BR_ProcessParam_0.Mode := mcPPM_READ;
	
	MC_BR_ProcessParam_0();
     
END_PROGRAM
